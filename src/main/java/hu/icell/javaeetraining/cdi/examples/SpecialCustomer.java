package hu.icell.javaeetraining.cdi.examples;

public class SpecialCustomer extends Customer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double discontPercentage;

	public double getDiscontPercentage() {
		return discontPercentage;
	}

	public void setDiscontPercentage(double discontPercentage) {
		this.discontPercentage = discontPercentage;
	}

}
