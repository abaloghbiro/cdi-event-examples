package hu.icell.javaeetraining.cdi.examples;

import java.io.IOException;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/customer")
public class CustomerServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private CustomerRepository repo;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		final HttpServletResponse r = resp;

		repo.getAllCustomer().forEach(c -> {
			try {
				r.getWriter().println("Customer : " + c.getFirstname() + " / " + c.getLastname());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String percentage = req.getParameter("percentage");

		Customer customer = new Customer();

		if (percentage != null && !percentage.isEmpty()) {
			customer = new SpecialCustomer();
			((SpecialCustomer) customer).setDiscontPercentage(Double.parseDouble(percentage));
		}

		customer.setFirstname(firstName);
		customer.setLastname(lastName);

		repo.addCustomer(customer);
	}

}
