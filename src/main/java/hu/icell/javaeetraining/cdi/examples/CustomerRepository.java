package hu.icell.javaeetraining.cdi.examples;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

@Stateful
public class CustomerRepository {

	@LogMessage
	@Inject
	private MessageService messageService;

	@Inject
	@Any
	private Instance<MessageService> messageServiceProvider;

	private final List<Customer> CUSTOMERS = new ArrayList<Customer>();

	public void addCustomer(Customer c) {

		CUSTOMERS.add(c);

		System.out.println("Is ambiguous ? : " + messageServiceProvider.isAmbiguous());
		System.out.println("Is unsatisfied ? : " + messageServiceProvider.isUnsatisfied());

		messageServiceProvider.forEach(service -> {

			System.out.println("Found service impl: " + service.getClass());
		});

		Instance<MessageService> subProvider = messageServiceProvider.select(new AnnotationLiteral<EventMessage>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		});

		if (!subProvider.isUnsatisfied()) {
			MessageService eventSenderMessageService = subProvider.get();
			eventSenderMessageService.sendMessage(c);
		}

		messageService.sendMessage(c);

	}

	public List<Customer> getAllCustomer() {
		return CUSTOMERS;
	}
}
