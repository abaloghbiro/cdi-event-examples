package hu.icell.javaeetraining.cdi.examples;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@EventMessage
@ApplicationScoped
public class EventMessageService implements MessageService {

	@Inject
	@SpecialCustomerCreation
	private Event<String> specialCustomerCreationEvent;

	@Inject
	private Event<String> normalCustomerCreationEvent;

	@Override
	public void sendMessage(Customer c) {

		final String message = c.getClass().getSimpleName() + " created: " + c.getFirstname() + " / " + c.getLastname();

		if (c instanceof SpecialCustomer) {
			specialCustomerCreationEvent.fire(message);
		} else {
			normalCustomerCreationEvent.fire(message);
		}

	}

}
