package hu.icell.javaeetraining.cdi.examples;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class CustomerObserver {

	public void listenToCustomerCreationEvent(@Observes String customerCreation) {

		System.out.println("CUSTOMER CREATION LISTENER: "+customerCreation);
	}

	public void listenOnlyToSpecialCustomerCreationEvent(@Observes @SpecialCustomerCreation String customerCreation) {

		System.out.println("SPECIAL CUSTOMER CREATION LISTENER: "+customerCreation);
	}
}
