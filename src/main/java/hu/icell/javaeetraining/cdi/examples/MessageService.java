package hu.icell.javaeetraining.cdi.examples;

public interface MessageService {

	void sendMessage(Customer customer);
}
