package hu.icell.javaeetraining.cdi.examples;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

@Retention(RUNTIME)
@Target(value = {ElementType.PARAMETER,ElementType.FIELD})
@Qualifier
public @interface SpecialCustomerCreation {

}
