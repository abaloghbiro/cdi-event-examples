package hu.icell.javaeetraining.cdi.examples;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;

@LogMessage
@ApplicationScoped
public class LoggerMessageService implements MessageService {

	Logger logger = Logger.getLogger("loggerMessageService");

	@Override
	public void sendMessage(Customer c) {

		final String message = c.getClass().getSimpleName() + " created: " + c.getFirstname() + " / " + c.getLastname();
		logger.info(message);
	}

}
